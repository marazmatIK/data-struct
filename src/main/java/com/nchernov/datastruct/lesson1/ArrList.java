package com.nchernov.datastruct.lesson1;

import java.util.*;

/**
 * Created by zugzug on 04.07.17.
 */
public class ArrList<T> implements RandomAccess, List<T> {
    private Object[] elements;
    private int capacity;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (elements == null) {
            elements = new Object[2];
            capacity = 2;
        }
        if (size >= capacity) {
            int newSize = capacity * 2;
            if (newSize < 0) {
                //throw new IllegalStateException("Maximum size exceeded: " + capacity);
                return false;
            }
            Object[] newElements = new Object[newSize];
            for (int i = 0; i < elements.length; i++) {
                newElements[i] = elements[i];
            }
            newElements[elements.length + 1] = t;
            capacity = newSize;
            elements = newElements;
        }
        elements[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public T get(int index) {
        return (T) elements[index];
    }

    @Override
    public T set(int index, T element) {
        elements[index] = element;
        return element;
    }

    @Override
    public void add(int index, T element) {
        if (elements == null) {
            elements = new Object[2];
            capacity = 2;
        }
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        int newSize = capacity;
        if (size >= capacity) {
            newSize = capacity * 2;
            if (newSize < 0) {
                throw new IllegalStateException("Maximum size exceeded: " + capacity);
            }
            capacity = newSize;
        }
        //TODO: may be optimized to not to create new arrays in case size < capacity
        Object[] newElements = new Object[newSize];
        int j = 0;
        for (int i = 0; i < elements.length && j < newElements.length; i++) {
            if (i == index) {
                newElements[j] = element;
                j++;
            }
            newElements[j] = elements[i];
            j++;
        }
        elements = newElements;
        size++;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        //int newSize = capacity - 1;
        //Object[] newElements = new Object[newSize];
        T element = (T) elements[index];
        //int j = 0;
        if (size <= 1) {
            elements[0] = null;
        } else {
            for (int i = index; i < elements.length; i++) {
                if (i < (size - 1)) {
                    elements[i] = elements[i + 1];
                }
            }
        }
        size--;
        return element;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}
