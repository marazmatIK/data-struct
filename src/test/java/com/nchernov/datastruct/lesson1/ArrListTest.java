package com.nchernov.datastruct.lesson1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by zugzug on 04.07.17.
 */
public class ArrListTest {
    @Test
    public void addSingle() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);

        assertEquals(1, arrList.size());
        assertEquals(1, (int) arrList.get(0));
    }

    @Test
    public void addMultiple() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(2);
        arrList.add(3);
        arrList.add(4);

        assertEquals(4, arrList.size());
        assertEquals(1, (int)arrList.get(0));
        assertEquals(2, (int)arrList.get(1));
        assertEquals(3, (int)arrList.get(2));
        assertEquals(4, (int)arrList.get(3));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addIntoZero() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1, 0);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addIntoLastIndexPlusOne() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(1, 2);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addIntoExceedingSizePlace() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1, 42);
    }

    @Test
    public void addIntoZeroWhenFirstElPresent() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(0, 2);


        assertEquals(2, arrList.size());
        assertEquals(2, (int)arrList.get(0));
        assertEquals(1, (int)arrList.get(1));
    }

    @Test
    public void addIntoLastPositionWhenFirstElPresent() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(arrList.size() - 1, 2);


        assertEquals(2, arrList.size());
        assertEquals(2, (int)arrList.get(0));
        assertEquals(1, (int)arrList.get(1));
    }

    @Test
    public void addIntoZeroPositionWhenMoreThanOneElPresent() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(3);
        arrList.add(0, 2);


        assertEquals(3, arrList.size());
        assertEquals(2, (int)arrList.get(0));
        assertEquals(1, (int)arrList.get(1));
        assertEquals(3, (int)arrList.get(2));
    }

    @Test
    public void addIntoLastPositionWhenMoreThanOneElPresent() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(3);
        arrList.add(arrList.size() - 1, 2);


        assertEquals(3, arrList.size());
        assertEquals(1, (int)arrList.get(0));
        assertEquals(2, (int)arrList.get(1));
        assertEquals(3, (int)arrList.get(2));
    }

    @Test
    public void removeFromEmptyList() {
        ArrList<Integer> arrList = new ArrList<>();
        assertNull(arrList.remove(0));
    }

    @Test
    public void removeFromUnexistentPosition() {
        ArrList<Integer> arrList = new ArrList<>();
        assertNull(arrList.remove(42));
    }

    @Test
    public void removeFromUnexistentPositionNegative() {
        ArrList<Integer> arrList = new ArrList<>();
        assertNull(arrList.remove(-42));
    }

    @Test
    public void removeFromListWithSingleItem() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        int element = arrList.remove(0);
        assertEquals(0, arrList.size());
        assertEquals(1, element);
        assertEquals(null, arrList.get(0));
    }

    @Test
    public void removeFromZeroPositionOfListWithMoreThanOneItems() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(2);
        int element = arrList.remove(0);
        assertEquals(1, arrList.size());
        assertEquals(1, element);
        assertEquals(2, (int)arrList.get(0));
    }

    @Test
    public void removeFromLastPositionOfListWithMoreThanOneItems() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(2);
        int element = arrList.remove(arrList.size() - 1);
        assertEquals(1, arrList.size());
        assertEquals(2, element);
        assertEquals(1, (int)arrList.get(0));
    }

    @Test
    public void removeThanAddToTheEnd() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(2);
        int element = arrList.remove(arrList.size() - 1);
        assertEquals(1, arrList.size());
        assertEquals(2, element);
        assertEquals(1, (int)arrList.get(0));

        arrList.add(3);
        assertEquals(2, arrList.size());
        assertEquals(3, (int)arrList.get(arrList.size() - 1));
    }

    @Test
    public void addThenRemoveTheLast() {
        ArrList<Integer> arrList = new ArrList<>();
        arrList.add(1);
        arrList.add(2);

        arrList.add(3);

        assertEquals(3, arrList.size());
        assertEquals(3, (int)arrList.get(arrList.size() - 1));

        int element = arrList.remove(arrList.size() - 1);
        assertEquals(2, arrList.size());
        assertEquals(3, element);
        assertEquals(2, (int)arrList.get(arrList.size() - 1));


    }
}
